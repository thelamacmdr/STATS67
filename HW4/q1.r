#source('./data.r')

# Part A
fit <- lm(fr_pop ~fr_years)
summary(fit)

#Estimate Intercept - B0 -198.163
#Estimate Slope - B1   0.12589r


# Part B
y <- 0.12598*fr_years - 198.164
mydat <- data.frame(y,fr_pop,fr_years)
matplot(mydat[,3],mydat[,2],type="p",pch=1,xlab="Years",ylab="Population")
matlines(mydat[,3],mydat[,1],type="l")

# Part C
F1 <- fr_pop[1:23]
F1_years <- fr_years[1:23]
F3 <- fr_pop[28:36]
F3_years <- fr_years[28:36]

fit_1 <- lm(F1~F1_years)
summary(fit_1)
fit_3 <- lm(F3~F3_years)
summary(fit_3)

# Part D
t <- (0.348 - 0.114)/0.017
p <- 2*pt(-abs(t),df=7)